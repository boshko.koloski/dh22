# DH2022 - New Folder TEAM

- Boshko Koloski
- Ilija Tavchioski

# Description

Application for easier disposal of waste, while optimizing the route delivery cost - enabling disposal truck sharing for waste AND simultaneously analyzing which of the electronical compliances can be of use for the ones in need.

### Automatic Extraction of Images

- We utilized the **Facebook DETR model** https://github.com/facebookresearch/detr Transformer model for feature extraction and labeling. 

- We built a custom dataset from https://bolha.com/ to scrape and organize images of electronics, in order to extract and classify compliances from the images of our users.

### Automatic assignment of reusable compliances 
- We built live database feed from https://podarimo.si/ & Twitter tweets that gathers information about people in need of **electronics compliances**. 

### Extraction of information from OPSI

- Explaratory data analysis of files containing keywords like **smeti, odlagalisca, odpadke**

-  We extracted and built ontology from the VOKA SNAGA https://www.vokasnaga.si/sites/www.jhl.si/files/dokumenti/izvlecek_cenika_blaga_in_storitev_velja_od_1._1._2020.pdf and cross-referenced it with the OPSI podatke from **GreenHACK**

## USED APIS:

### Twitter API
### TransformerHUB API
### GeoLocator API
### OpenStreetMap API
### OPTIMDATA API

# DEMO
## Insert your waste count and include a picture of the waste for automatic detection
![ALT](/readme_pics/1.png)
## Detection of electronic compliances suitable for giving further rather than scraping them
![ALT](/readme_pics/2.png)
## Via semantical AI/ML search, find the sutable users for the useful electronics, places for gifting of the equipment are found
![ALT](/readme_pics/4.png)
## The request is added to the system and a proposed route is shown, with the gains
![ALT](/readme_pics/5.png)


# DEMO2 
## Detection of electronic compliances suitable for giving further rather than scraping them
![ALT](/readme_pics/21.png)
## Via semantical AI/ML search, find the sutable users for the useful electronics, places for gifting of the equipment are found
![ALT](/readme_pics/22.png)
