from re import sub
import numpy as np
from numpy.core.defchararray import lower
from sympy import O
import streamlit as st
import pandas as pd
from geopy.geocoders import Nominatim
from obj_det import labels, check_feasble
import geopy.distance
from google.cloud import firestore
import random 
import datetime
def find_compatible():
    doc_ref = db.collection("users")
    df = pd.concat(pd.DataFrame(f.to_dict()) for f in doc_ref.get())#.astype(str)
    df = df[df['type']=='raw'].reindex() 
    orig_place = (46.0757492,14.5045035)
    df['start_date'] = pd.to_datetime(df['pickup_date']).astype(int) / 10**9
    df['earliest_date'] = pd.to_datetime(df['latest_date']).astype(int) / 10**9
    interval = [pd.Interval(x, y, closed = 'both') for x,y in zip(df['start_date'],df['earliest_date'])]
    df['intervals'] = interval
    optims = []
    weights = df['weight'].to_list()
    coords = list(zip(df['lat'],df['lon']))
    seed = 313
    optims = []
    random.seed(seed)

    for i in range(len(interval)):
        for j in range(len(interval)):
            if i<j:
                i1 = interval[i]
                i2 = interval[j]
                sum_ = weights[i] + weights[j]
                if(i1.overlaps(i2) and sum_ <= 3500):
                    final_kms =  geopy.distance.geodesic(coords[i],coords[j]).km+ geopy.distance.geodesic(coords[i],orig_place).km + geopy.distance.geodesic(coords[j],orig_place).km
                    if(final_kms < 15):
                        best_date = random.uniform(max(interval[i].left, interval[j].left), min(interval[i].right, interval[j].right))
                        date_time = datetime.datetime.fromtimestamp(best_date).strftime('%Y-%m-%d')
                        optims.append([sum_,-final_kms,df['name'].to_list()[i],df['name'].to_list()[j],date_time])

    optims.sort(reverse=True)
    return optims[0]

path = "dh2022-dcd61-firebase-adminsdk-ov2fl-f41fe548f5.json"
db = firestore.Client.from_service_account_json(path)

st.title('GARBAGE SHUTTLE')

st.title('Waste Disposal Sharing, done right!')
st.header('Enter your disposal info.')

with st.form("my_form", clear_on_submit = True):
    found_stuff_dic = {}
    found_images = {}
    qualities = {}
    options = ['raw']
    name = st.text_input("Name")
    adress = st.text_input("Street")
    num = st.number_input("Street number")
    manucipality = st.text_input("Manucipality")
    weight = st.number_input("Mass of the waste in kgs.")
    
    uploaded_file = st.file_uploader("Upload a picture.")
    
    earliest_pickup = st.date_input("Earliset pickup date")    
    latest_pickup = st.date_input("Latest pickup date")
    try:
        locator = Nominatim(user_agent="myGeocoder")
        location = locator.geocode(f"{adress} {num}, {manucipality}, Slovenia")
        lat, long = location.latitude, location.longitude
    except:
        lat, long = 0,0
    submitted = st.form_submit_button("Submit")
        
if submitted:
    if uploaded_file is not None:
        st.header("Extraction of reusable appliances")
        bytes_data = uploaded_file.getvalue()
        st.image(bytes_data)
        lbls, found_stuff = labels(bytes_data, return_pics=True)
        for l, img in zip(lbls, found_stuff):
            GOLD_LBLS = ['refrigerator','oven','microwave']
            if l in GOLD_LBLS:
              #  st.caption(l)
              #  st.image(img)
                
                qualities[l] = check_feasble(img, l)[0]
             #   st.write(quality)
                found_stuff_dic[l] =  1 + found_stuff_dic.get(l, 0)
                found_images[l] = img

    ccols = st.columns(3)
    is_reusable = []
    for i, lbl in enumerate(['refrigerator','oven','microvawe']):
        if lbl in found_stuff_dic:
            ccols[i].metric(lbl, str(found_stuff_dic[lbl]),  str(found_stuff_dic[lbl]))
            ccols[i].image(found_images[lbl])
            if qualities[lbl] > 0.5:
                options.append(lbl)
                st.header(f"FOUND PLACEMENT FOR {lbl}")
                st.table(pd.read_csv(f"data/give/{lbl[0].upper()}.csv",usecols=['link','region']).sample(1))
        else:
            ccols[i].metric(lbl, '0')#, '0')    

        
    output_dict = {"name":name,
                    "weight" : weight,
                    "adress" : adress,
                    "nr" : num,
                    "lat": lat,
                    "lon": long,
                    "mun" : manucipality,
                    "type" : options,
                    "pickup_date" : str(earliest_pickup),
                    "latest_date" : str(latest_pickup)}
    db.collection(u"users").add(output_dict)


but = st.button('SAVE THE PLANET AND YOUR MONEY button.')

if but:
    _,final_kms,N1, N2, date_time = find_compatible()
    storitvena_ura = 28.50
    vozilo_ura = 69.25
    cena_r = random.uniform(0.33, 0.65)

    eur = 33

    price = int(-final_kms*34)
    final_cena =  storitvena_ura +  vozilo_ura + cena_r * price
    final_cena = np.round(final_cena)
    st.header(f'Path combination with  **{N1} & {N2}**, optimal date **{date_time}**, price save: **{final_cena}** EUR :euro: :euro: :euro: :euro:')

    co2_emmision =  180.5 * (-final_kms)/1000
    st.header(f'Saved {eur}% in money and reduced {np.round(co2_emmision)} kg/km :herb: :seedling: :blossom:')

    doc_ref = db.collection("users")
    df = pd.concat(pd.DataFrame(f.to_dict()) for f in doc_ref.get()).astype(str)

    #st.table(df)
    df['lat'] = df['lat'].astype(float)
    df['lon'] = df['lon'].astype(float)
    st.map(df)

if submitted:
    #st.snow()

    from PIL import Image
    image = Image.open('maxresdefault.jpg')

    st.image(image, caption='GO GREEEEEEEEEEEEEEEEN!')
    st.header("GO GREEN, GO ZERO WASTE!")
    st.header("NEW FOLDER(4) TEAM")
