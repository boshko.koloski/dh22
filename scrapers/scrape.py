
import requests
from bs4 import BeautifulSoup
import pandas as pd
from PIL import Image
from io import BytesIO
from matplotlib import pyplot as plt
from imageio import imread
import shutil

locations = []
pictures = []
prices = []
names = []

for i in range(1, 2):

    r = requests.get("https://www.bolha.com/pralni-stroji?page=" + str(i))
    print("https://www.bolha.com/pecice?page=" + str(i))
    soup = BeautifulSoup(r.content, 'html5lib') 
    list = soup.findAll("li", attrs={
        "class" : "EntityList-item",
        "class" : "EntityList-item--Regular",
        "class" : "EntityList-item--n6",
        "class" : "bp-radix__faux-anchor"
    })

    for li in list:
        if li.find("a", attrs={"class" : "link"}).get("href")[:14] == "/pralni-stroji":
            newurl = "https://www.bolha.com" + li.find("a", attrs={"class" : "link"}).get("href")
        
            r = requests.get(newurl)
            
            soup = BeautifulSoup(r.content, 'html5lib')
            gallery = soup.findAll("li", attrs={
                "class" : "BaseEntityThumbnails-item",
                "class" : "Gallery-thumbnailsItem",
                "class" : "Gallery-thumbnailsItem--media" 
            })
            cnt = 0
            for img in gallery:
                cnt += 1
                src = img.find("a", attrs={
                    "class" : "link",
                    "class" : "is-active",
                    "class" : "BaseEntityThumbnails-link",
                    "class" : "Gallery-thumbnail"
                })
                src = src.get("href")
                src = "https:" + src
                filename = "./pralni/" + li.find("a", attrs={"class" : "link"}).text.split('/')[0] + str(cnt) + ".jpg"

                r = requests.get(src, stream = True)
                r.raw.decode_content = True
    
                # Open a local file
                with open(filename,'wb') as f:
                    shutil.copyfileobj(r.raw, f)

            locations.append(li.find("div", attrs={
                "class" : "entity-description-main"
            }).text)
            prices.append(li.find("strong", attrs={
                "class" : "price",
                "class" : "price--hrk"
            }).text)
            names.append(li.find("a", attrs={"class" : "link"}).text.split('/')[0])

columns = ["name", "location", "price"]
df = pd.DataFrame()
df["name"] = names
df["location"] = locations
df["price"] = prices
df.to_csv("./data/ovens.csv", index=False)     
