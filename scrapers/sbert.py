import pandas as pd
import numpy as np
import os
from sentence_transformers import SentenceTransformer, util
from PIL import Image
from os import listdir

model = SentenceTransformer('clip-ViT-B-32')
def avg_cosine_similarity(d1, path):
    avg = 0
    data = np.loadtxt(path, dtype=np.float32)
    for i in range(len(data)):
        
        avg += util.cos_sim(d1, data[i])
    avg /= len(data)
    return avg
def embedd_ovens(path="./pralni"):
    data = {}
    
    list = []
    for image_name in listdir(path):
        list.append(Image.open(os.path.join(path, image_name)))

    embeds = model.encode(list)

    np.savetxt("./data/embed_pralni.txt", embeds)
def embedd_img(img):
    return SentenceTransformer('clip-ViT-B-32').encode(img)
def embedd_text(text):
    return SentenceTransformer('clip-ViT-B-32').encode([text])


if __name__ == "__main__":
    embedd_ovens()
    #print(avg_cosine_similarity(embedd_img(Image.open("./refrigirators/1 leto star HLADILNIK GORENJE4.jpg")), path="./data/embed_ref.txt"))
    
