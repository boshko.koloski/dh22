"""


col1, col2, col3 = st.columns(3)


with col1:
    st.header("A cat")
    title = st.text_input('Movie title', 'Life of Brian')
    st.write('The current movie title is', title)
    st.image("https://static.streamlit.io/examples/cat.jpg")

with col2:
    st.header("A dog")
    st.image("https://static.streamlit.io/examples/dog.jpg")

with col3:
    st.header("An owl")
    st.image("https://static.streamlit.io/examples/owl.jpg")

"""
"""

-- find if a transfer of thing is suitable
-- employ stuff as taking photo or something --> automatic detection and list fill in input: picture -> output type -> [is_usable?, type?] [DONE]
-- arange free transfer of GIFTED stuff with fullfilfment of the paid KOSOVNI odpatki stuff 
-- automatic detection of stufff [DONE]
"""

from sentence_transformers import SentenceTransformer, util
from transformers import DetrFeatureExtractor, DetrForObjectDetection
from PIL import Image
import io
import torch
import numpy as np

def labels(im, return_pics = False):
    im = Image.open(io.BytesIO(im))#(im)
    feature_extractor = DetrFeatureExtractor.from_pretrained("facebook/detr-resnet-50")
    model = DetrForObjectDetection.from_pretrained("facebook/detr-resnet-50")

    encoding = feature_extractor(im, return_tensors="pt")
    print(len(encoding))
    outputs = model(**encoding)
    probas = outputs.logits.softmax(-1)[0, :, :-1]
    keep = probas.max(-1).values > 0.9
    m_dict = model.config.id2label
    lbls = [m_dict[p.argmax().item()] for p in probas[keep]] 
    if return_pics:
        target_sizes = torch.tensor(im.size[::-1]).unsqueeze(0)
        postprocessed_outputs = feature_extractor.post_process(outputs, target_sizes)
        bboxes_scaled = postprocessed_outputs[0]['boxes'][keep].tolist()
        crops = [im.crop(b)  for b in bboxes_scaled]
        return lbls,crops
    return lbls

def avg_cosine_similarity(img, path):
    avg = 0
    clip_model = SentenceTransformer('clip-ViT-B-32')
    d1 = clip_model.encode(img)
    data = np.loadtxt(path, dtype=np.float32)
    for i in range(len(data)):        
        avg += util.cos_sim(d1, data[i])
    avg /= len(data)
    return avg

def check_feasble(img, label):
    mapper = {"oven":"embeds_oven.txt", 
              "refrigerator":"embed_ref.txt",
              "microwave":"embed_pralni.txt"}
    path = f"data/{mapper[label]}"
    return avg_cosine_similarity(img,path)


